package co.youplus.stub;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import java.util.Calendar;

import co.youplus.stub.receivers.InactiveUserHandlerAlarmReceiver;
import co.youplus.stub.services.PackageWatchdogService;

public class Utils {

    public static void startPackageWatchdogService(Context context) {
        context.startService(new Intent(context, PackageWatchdogService.class));
    }

    // inactiveDaysCount is the count of days the user is inactive
    public static int getInactiveDaysCount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_NAME, Context.MODE_PRIVATE);
        return preferences.getInt(Constants.INACTIVE_DAYS_COUNT, 0);
    }

    public static void setInactiveDaysCount(Context context, int inactiveNotificationCount) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Constants.INACTIVE_DAYS_COUNT, inactiveNotificationCount);
        editor.apply();
    }

    // inactiveNotificationCount is the count of reminder notifications we send to inactive users
    public static int getInactiveNotificationCount(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_NAME, Context.MODE_PRIVATE);
        return preferences.getInt(Constants.INACTIVE_NOTIFICATION_COUNT, 0);
    }

    public static void setInactiveNotificationCount(Context context, int inactiveNotificationCount) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(Constants.INACTIVE_NOTIFICATION_COUNT, inactiveNotificationCount);
        editor.apply();
    }

    // a user is active if they open our app
    public static boolean isUserActive(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(Constants.IS_USER_ACTIVE, false);
    }

    public static void setUserActive(Context context, boolean isUserActive) {
        SharedPreferences preferences = context.getSharedPreferences(Constants.APP_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Constants.IS_USER_ACTIVE, isUserActive);
        editor.apply();
    }

    // this alarm is set for 11 AM every day and handles sending notifications to inactive users
    public static void setInactiveUserHandlerAlarm(Context context) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 11);

        Intent intent = new Intent(context, InactiveUserHandlerAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    // send the (inactive) user a local notification to remind them to open the app
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void sendReminderNotification(Context context) {
        int notificationId = 001;
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Notification notification = new Notification.Builder(context)
                .setContentTitle(context.getString(R.string.app_name_title))
                .setContentText(getReminderNotificationText(context))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notificationId, notification);

        Utils.setInactiveNotificationCount(context, Utils.getInactiveNotificationCount(context) + 1);

    }

    // returns the appropriate reminder notification message based on inactiveNotificationCount
    public static String getReminderNotificationText(Context context) {
        int inactiveNotificationCount = Utils.getInactiveNotificationCount(context);
        switch (inactiveNotificationCount) {
            case 0:
                return context.getString(R.string.notification_text_1);
            case 1:
                return context.getString(R.string.notification_text_2);
            case 2:
                return context.getString(R.string.notification_text_3);
            case 3:
                return context.getString(R.string.notification_text_4);
            default:
                return context.getString(R.string.notification_text_1);
        }
    }
}

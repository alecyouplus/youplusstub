package co.youplus.stub;

public class Constants {

    public static final String APP_NAME = "co.youplus.stub";

    public static final String INACTIVE_DAYS_COUNT = "inactive_days_count";
    public static final String INACTIVE_NOTIFICATION_COUNT = "inactive_notification_count";
    public static final String IS_USER_ACTIVE = "is_user_active";

}

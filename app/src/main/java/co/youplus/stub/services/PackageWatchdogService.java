package co.youplus.stub.services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import co.youplus.stub.Utils;

/*
*
* This service constantly monitors active packages on the device and performs an action once it
* detects one of the apps we want to track has been launched
*
 */

public class PackageWatchdogService extends Service {

    public static String TAG = PackageWatchdogService.class.getSimpleName();
    public static final long WATCHDOG_POLL_DELAY = 1000;

    private Context context;
    private Handler handler;
    private ActivityManager activityManager;

    private static ArrayList<String> watchdogPackageNames = new ArrayList();

    static {
        watchdogPackageNames.add("co.youplus.stub");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        this.context = getApplicationContext();
        activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                watchActivePackages();
            }
        };

        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(WATCHDOG_POLL_DELAY);
                        handler.sendEmptyMessage(0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    /*
    *
    * fetch the device's active packages every second and see if any of the package names we are
    * tracking are included in the list
    *
    */
    public void watchActivePackages() {
        String[] activePackages;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            activePackages = getActivePackages();
        } else {
            activePackages = getActivePackagesCompat();
        }
        if (activePackages != null) {
            for (String activePackage : activePackages) {
                if (watchdogPackageNames.contains(activePackage)) {
                    // flag the user as active since they opened the app
                    Utils.setUserActive(context, true);
                    Utils.setInactiveDaysCount(context, 0);
                    Utils.setInactiveNotificationCount(context, 0);
                }
            }
        }
    }

    // get active packages using deprecated getRunningTasks method
    // (http://stackoverflow.com/a/27140347)
    private String[] getActivePackagesCompat() {
        final List<ActivityManager.RunningTaskInfo> taskInfo = activityManager.getRunningTasks(1);
        final ComponentName componentName = taskInfo.get(0).topActivity;
        final String[] activePackages = new String[1];
        activePackages[0] = componentName.getPackageName();
        return activePackages;
    }

    private String[] getActivePackages() {
        final Set<String> activePackages = new HashSet<String>();
        final List<ActivityManager.RunningAppProcessInfo> processInfos = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : processInfos) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                activePackages.addAll(Arrays.asList(processInfo.pkgList));
            }
        }
        return activePackages.toArray(new String[activePackages.size()]);
    }
}
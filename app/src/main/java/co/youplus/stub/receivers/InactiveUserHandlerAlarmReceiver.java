package co.youplus.stub.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import co.youplus.stub.Utils;

public class InactiveUserHandlerAlarmReceiver extends BroadcastReceiver {

    /*
    *
    * This class handles the actions that are to be performed for inactive users (reminder notifications)
    *
     */

    @Override
    public void onReceive(Context context, Intent intent) {

        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        int inactiveDaysCount = Utils.getInactiveDaysCount(context);
        if (inactiveDaysCount < 2) {
            Utils.setInactiveDaysCount(context, inactiveDaysCount + 1);
        }
        if (inactiveDaysCount == 2) {
            // flag the user as inactive if they haven't opened the app within 2 days
            Utils.setUserActive(context, false);
        }
        if (day == Calendar.FRIDAY && !Utils.isUserActive(context) &&
                Utils.getInactiveNotificationCount(context) < 4) {
            // send a reminder notification if it's Friday, the user is inactive,
            // and we haven't sent more than 4 notifications already
            Utils.sendReminderNotification(context);
        }
    }

}
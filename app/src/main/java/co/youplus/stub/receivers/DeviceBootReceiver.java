package co.youplus.stub.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import co.youplus.stub.Utils;

public class DeviceBootReceiver extends BroadcastReceiver {

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            // when the device boots, re-start the PackageWatchdogService and
            // re-set the repeating alarm to handle actions for inactive users
            Utils.startPackageWatchdogService(context);
            Utils.setInactiveUserHandlerAlarm(context);
        }
    }
}
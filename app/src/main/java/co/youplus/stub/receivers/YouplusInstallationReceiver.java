package co.youplus.stub.receivers;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

public class YouplusInstallationReceiver extends BroadcastReceiver {

    public static String TAG = YouplusInstallationReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String packageName = intent.getData().getEncodedSchemeSpecificPart();
        Log.d(TAG, "Packaged addded: " + packageName);
        if (packageName.equals("co.youplus")) {
            hideAppLauncherIcon(context);
        }
    }

    // hides the launcher icon of the stub so the user feels like it has been uninstalled
    private void hideAppLauncherIcon(Context context) {
        context.getPackageManager().setComponentEnabledSetting(
                ComponentName.unflattenFromString("co.youplus.stub/co.youplus.stub.MainActivity"),
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
    }
}

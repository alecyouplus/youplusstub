package co.youplus.stub.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class YouplusRemovalReceiver extends BroadcastReceiver {

    public static String TAG = YouplusInstallationReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String packageName = intent.getData().getEncodedSchemeSpecificPart();
        Log.d(TAG, "Packaged removed: " + packageName);
        if (packageName.equals("co.youplus")) {
            // TODO: perform an action when the user uninstalls the Youplus app (display a survey?)
        }
    }

}

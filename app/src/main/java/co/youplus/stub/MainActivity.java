package co.youplus.stub;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openPlayStore();
        Utils.startPackageWatchdogService(this);
        Utils.setInactiveUserHandlerAlarm(this);
        finish();
    }

    private void openPlayStore() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=co.youplus"));
        startActivity(intent);
    }

}
